def hidden_singles(domain):
    temp = [] # example: [[1,False],[2,False],[6,True]] false means not the one

    for i in domain:
        for j in i:
            if [j, True] in temp:
                x = temp.index([j, True])
                temp[x] = [j, False]
            elif (not ([j, True] in temp)) and (not ([j, False] in temp)):
                temp.append([j, True])

    for i in temp:
        if i[1] is True:
            x = i[0]
            break

    for i in domain:
        for j in i:
            if x is j:
                domain[i] = [x]

    return domain

domain = [[1, 5, 9], [1, 5, 6, 9], [1, 9], [1, 5, 9]]
print(hidden_singles(domain))