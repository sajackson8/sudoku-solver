########################################################################################################################
# DEVELOPED BY: Sha Jackson, Chris Bodde, Joshua Gatpandan
# CONTACT INFO: shajack2000@gmail.com, boddeck@gmail.com, joshuagatpandan@gmail.com
# ASSIGNMENT: CSCE 405 - Assignment 4, Sudoku Solver
#
# PURPOSE:
#
# STRATEGIES:
#
########################################################################################################################
import copy

# EMERGENCY STACK SIZE!!!
# sys.setrecursionlimit(2000)


class Sudoku:

    def __init__(self, in_file="testSudoku.txt"):
        self.puzzle = []
        self.in_file = in_file
        self.solvedBoard = []

        self.iterations = 0

    def __str__(self):
        print(self.puzzle)

    # reads given text file to a 2d array with newlines indicating each row and space being each tile
    def init_puzzle(self):
        file = open(self.in_file, "r")
        file_text = file.read()
        file_text = file_text.split('\n')
        
        for row in file_text:
            row = row.split()
            new_board_row = []
            for num in row:
                new_board_row.append(int(num))
            self.puzzle.append(new_board_row)

    def print_initial_board(self):
        for row in self.puzzle:
            print(row)
    
    def print_final_board(self):
        for row in self.solvedBoard:
            print(row)
    
    # given one of the tiles on the board return the list of possible numbers that could be there
    def get_possible_num(self, x, y):
        list_of_num = []
        for num in range(1, 10):
            square_check = True
            row_check = True
            column_check = True
            
            for i in range(0, 9):
                if self.puzzle[y][i] == num:
                    row_check = False

            for i in range(0, 9):
                if self.puzzle[i][x] == num:
                    column_check = False

            grid_x = (x//3)*3
            grid_y = (y//3)*3
            for i in range(0, 3):
                for j in range(0, 3):
                    if self.puzzle[grid_y+i][grid_x+j] == num:
                        square_check = False
                        
            if square_check and row_check and column_check:
                list_of_num.append(num)

        if len(list_of_num) < 1:
            return False
        return list_of_num

    def find_hidden_solo(self, domains):
        list_of_counts = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        new_group = []
        for domain in domains:
            for variable in domain:
                list_of_counts[variable] += 1

        for domain in domains:
            for varNum, numCountOfVariable in enumerate(list_of_counts):
                if numCountOfVariable == 1 and varNum in domain:
                    domain = [varNum]
                    break
            new_group.append(domain)
        return new_group

    def find_naked_pairs(self, domains_of_group):
        for item in domains_of_group:
            if len(item) == 2:
                if domains_of_group.count(item) > 1:
                    naked_pair_set = set(item)

                    for index, tempItem in enumerate(domains_of_group):
                        temp_set = set(tempItem)
                        new_domain = temp_set - naked_pair_set
                        new_domain = list(new_domain)

                        if not domains_of_group[index] == item:
                            domains_of_group[index] = new_domain

        return domains_of_group

    def find_naked_triples(self, domains):
        candidates = []

        # Naked Triples Technique is not Possible Without at Least 4 Cells
        if len(domains) < 4:
            return domains

        # Locate the Cells containing 3 or Less Possible Numbers
        for domain in domains:
            if len(domain) < 4:
                candidates.append(domain)

        # Find a group of 3 cells that together contain no more than 3 different numbers
        i = 0
        triple_nums = []
        triple = []
        while i < len(candidates):
            triple_nums.extend(j for j in candidates[i] if j not in triple_nums)
            triple.append(candidates[i])
            for q, elem in enumerate(candidates):
                if q != i:
                    if len(list(set(triple_nums) - set(elem)) + list(set(elem) - set(triple_nums))) < 3 - len(triple_nums):
                        triple_nums.extend(k for k in elem if k not in triple_nums)
                        triple.append(elem)
            if len(triple) < 3:
                i += 1
                triple = []
                triple_nums = []
            else:
                while len(triple) > 3:
                    triple.pop(len(triple) - 1)
                break

        for elem in domains:
            if elem not in triple:
                for num in triple_nums:
                    if num in elem:
                        elem.remove(num)

        return domains

    def reduce_domains(self):
        # Rows
        for row in range(9):
            domains_in_group = []
            for column in range(9):
                if not isinstance(self.domain_board[row][column], int):
                    domains_in_group.append(self.domain_board[row][column])
            self.find_hidden_solo(domains_in_group)
            self.find_naked_pairs(domains_in_group)
            self.find_naked_triples(domains_in_group)

        # Columns
        for column in range(9):
            domains_in_group = []
            for row in range(9):
                if not isinstance(self.domain_board[row][column], int):
                    domains_in_group.append(self.domain_board[row][column])
            self.find_hidden_solo(domains_in_group)
            self.find_naked_pairs(domains_in_group)
            self.find_naked_triples(domains_in_group)

        # Boxes
        for h_block in range(3):
            for v_block in range(3):
                domains_in_group = []
                for column in range(3):
                    for row in range(3):
                        if not isinstance(self.domain_board[row + (h_block * 3)][column + (v_block * 3)], int):
                            domains_in_group.append(self.domain_board[row + (h_block * 3)][column + (v_block * 3)])
                self.find_hidden_solo(domains_in_group)
                self.find_naked_pairs(domains_in_group)
                self.find_naked_triples(domains_in_group)

    def is_full(self):
        for y in range(0, 9):
            for x in range(0, 9):
                if self.puzzle[y][x] == 0 or isinstance(self.puzzle[y][x], list):
                    return False
                
        return True
              
    def solve(self):

        self.iterations += 1
        if self.solvedBoard:
            return

        # initialized this way so the first list of options is always set as most_constrained
        most_constrained = ([0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])

        self.domain_board = copy.deepcopy(self.puzzle)

        # check all positions on the board and find the variable with the least amount of possible numbers it could be
        for y in range(0, 9):
            for x in range(0, 9):
                if self.puzzle[y][x] == 0:
                    self.domain_board[y][x] = self.get_possible_num(x, y)
                    if not self.domain_board[y][x]:
                        return False

        self.reduce_domains()

        for row in range(9):
            for column in range(9):
                if not isinstance(self.domain_board[row][column], int):
                    if len(most_constrained[1]) > len(self.domain_board[row][column]):
                        most_constrained = ([column, row], self.domain_board[row][column])

        while most_constrained[1]:
            
            new_num = most_constrained[1].pop()
            new_x = most_constrained[0][0]
            new_y = most_constrained[0][1]
            self.puzzle[new_y][new_x] = new_num
            
            # if the board is full and evaluated as completed then save it as the solved board
            if self.is_full():
                if self.eval_solution(self.puzzle):
                    self.solvedBoard = copy.deepcopy(self.puzzle)
                return True
            
            self.solve()
            self.puzzle[new_y][new_x] = 0

    def eval_solution(self, solved_puzzle):

        # CHECK ROWS
        for row in range(9):
            rem_nums = [1, 2, 3, 4, 5, 6, 7, 8, 9]
            for column in range(9):
                if solved_puzzle[row][column] in rem_nums:
                    rem_nums.remove(solved_puzzle[row][column])
                else:
                    return False

        # CHECK COLUMNS
        for column in range(9):
            rem_nums = [1, 2, 3, 4, 5, 6, 7, 8, 9]
            for row in range(9):
                if solved_puzzle[row][column] in rem_nums:
                    rem_nums.remove(solved_puzzle[row][column])
                else:
                    return False

        # CHECK SQUARE
        for h_block in range(3):
            for v_block in range(3):
                vals = []
                for column in range(3):
                    for row in range(3):
                        vals.append(solved_puzzle[row+(h_block*3)][column+(v_block*3)])
                if sorted(vals) != [1, 2, 3, 4, 5, 6, 7, 8, 9]:
                    return False

        return True


def main():

    file = input("enter a file name: ")

    sudoku_puzzle = Sudoku(file)
    sudoku_puzzle.init_puzzle()

    print('\n--Initial Puzzle--\n')
    sudoku_puzzle.print_initial_board()

    print('\n--Solved Puzzle--\n')
    sudoku_puzzle.solve()
    sudoku_puzzle.print_final_board()
    print("num of iterations: ",sudoku_puzzle.iterations)


main()
